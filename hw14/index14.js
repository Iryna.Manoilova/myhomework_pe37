function arrayProcessing(array, element = document.body){

    function createList(array){
        let result = '<ul>';
        array.map(function(item) { 
            if (Array.isArray(item)) {
                result += `<li>${createList(item)}</li>` 
            } else {
                result += `<li>${item}</li>`;
            }     
        }); 
        result += '</ul>';   
        return result; 
    }    
    
    function appendList(element) {
        if (element === document.body) {
        } else {
            element = document.createElement(element);
            document.body.append(element);
        }
        element.innerHTML = createList(array);
        cleanUpElement(element);
    }

    function cleanUpElement(element){
        let seconds = document.createElement('H1');
        seconds.id = "timer";
        seconds.innerHTML = 3;
        element.append(seconds);

        let count = seconds.textContent;
        let countdown = setInterval(function() {
            console.log(count)
            count--;
            seconds.innerHTML = count;
            if (count === 0) {
                console.log(`element before remove ${element}`)
                element.remove();
                console.log(`element after remove ${element}`)
                clearInterval(countdown)
            };
        }, 1000);
    }

    appendList(element);
       
}
let newArray = ['new', ['fast', 'Irina', 67], 'move', 36];

arrayProcessing(newArray, 'div');
