
$(document).ready(function(){
   
    $(".js-scroll-menu a").click(function(e) {
        e.preventDefault();
        let id  = $(this).attr('href');
        let top = $(id).offset().top; 
        $('body, html').animate({scrollTop: top}, 800); 
    });

    $(window).scroll(function () { 
        if ($(this).scrollTop() > window.innerHeight){ 
            $('#top').fadeIn();
        } else {
            $('#top').fadeOut();
        }
      });
      $('#top').click(function () { 
        $('body, html').animate({
          scrollTop: 0
        }, 1500);
      });


      $(".js-btn-grey").click(function(){
        $(".toggle-container").slideToggle();
      });
    

});
  

