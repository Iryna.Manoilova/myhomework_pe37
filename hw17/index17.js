let togglers = document.querySelectorAll('[data-icon]');
let password = document.getElementById('password')
let confirm_password = document.getElementById('confirm');
const submit = document.getElementById('submit');
const error = document.getElementById('error');


const showHidePassword = function(){
    let icon = this.getAttribute('data-icon');
    let inputPassword = document.querySelector(`[data-password=${icon}]`);

    if (this.classList.contains("fa-eye")) {
        this.classList.remove("fa-eye");
        this.classList.add("fa-eye-slash")
        inputPassword.setAttribute('type', 'text'); 
    } else {
        this.classList.remove("fa-eye-slash");
        this.classList.add("fa-eye")
        inputPassword.setAttribute('type', 'password'); 
    }
};

function checkPasswords(){
    error.classList.remove('active')
    submit.disabled = (password.value && confirm_password.value) ? false : true;
}


function validatePassword() {
    if(password.value != confirm_password.value) {
        error.classList.add('active');
    } else {
        alert("You are welcome!")
    } 
}

password.onkeyup = checkPasswords;
confirm_password.onkeyup = checkPasswords;
submit.addEventListener('click', validatePassword);
Array.from(togglers).forEach( item => {
    item.addEventListener('click', showHidePassword);
}); 
