const link = document.createElement('link')
link.rel = 'stylesheet'  
link.href = './style_light.css'
const head = document.querySelector('head')

document.querySelector("#switch").onclick = function(e) {
    if (localStorage.getItem("theme") === null) {
        head.append(link)
        localStorage.setItem("theme", "light");
    } else {
        localStorage.removeItem("theme");
        head.removeChild(link)
    };
  };
  
  if (localStorage.getItem("theme") === "light") {
    head.append(link)
  }