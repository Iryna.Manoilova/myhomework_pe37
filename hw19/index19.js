const intervalTime = 3000

let images = document.querySelectorAll('.images-wrapper > img')
let currentImage = 0;
let slideInterval = setInterval(nextSlide,intervalTime);

let timeCounter= intervalTime;
let showTimeEl = document.querySelector('.time');
let updateTime = 50
let timeInteval = setInterval(backwordTimer, updateTime)
 let isPaused = false;

function backwordTimer(){
    showTimeEl.innerText = (timeCounter/1000).toFixed(2)
    timeCounter = timeCounter - updateTime 
    if (timeCounter <= 0){
        timeCounter = intervalTime
    }
}

function nextSlide(){
	images[currentImage].classList.remove('active');
	currentImage = (currentImage+1)%images.length;    
	images[currentImage].classList.add('active');
}

let pauseButton = document.getElementById('stop');
let playButton = document.getElementById('play')

function pauseSlideshow(){
	clearInterval(slideInterval);
    clearInterval(timeInteval);
    isPaused = true;
}

function playSlideshow(){
    if(isPaused){
        slideInterval = setInterval(nextSlide,intervalTime);
        timeCounter = intervalTime;
        timeInteval = setInterval(backwordTimer, updateTime);
        isPaused = false
    }
}

pauseButton.onclick = function(){
	pauseSlideshow();

};
playButton.onclick= function(){
	playSlideshow(); 
};
