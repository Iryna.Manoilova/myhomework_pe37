const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

  let bookList = books.map(({author,name,price})=>{

    try{
        if (author && name && price){
            return ` <li class="new"> Автор:${author} </li>
                <li> Имя:${name} </li>
                <li class ="last" > Цена:${price} </li>
            `
        }
        else if (!author){
          throw new Error('Отсутсвует автор')
        }
        else if (!name){
        throw new Error('Отсутсвует имя')
        }
        else if (!price){
        throw new Error('Отсутсвует цена')
        } 
    }
    catch (err){
        console.error(`Error: ${err.message}`);
    }
  })
  function addList(arr, el){
    el.insertAdjacentHTML('beforeend',`<ul>${arr}</ul>`)
  }
  addList(bookList, document.getElementById('root'))