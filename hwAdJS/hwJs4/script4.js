const filmUrl = 'https://ajax.test-danit.com/api/swapi/films'

const filmList = fetch(filmUrl)
  .then(response => response.json())
  .then(elements => {
    const filmsBox = document.createElement('ul')
    document.body.insertAdjacentElement('afterbegin',filmsBox)
    elements.forEach(films => {
      const {episodeId,openingCrawl,name,characters} = films
      const filmList = document.createElement('li')
      filmList.insertAdjacentHTML('beforeEnd',`
      <h2>Film №${episodeId}: ${name} </h2>
                  <p>${openingCrawl} </p>
                  `)

    
actorsFilm = characters.map(item => fetch(item))
const  boxActors =document.createElement('ul')

Promise.all(actorsFilm)
.then(responses => Promise.all(responses.map(el => el.json())))

.then(res=> {
    res.forEach(actor=>{
        const {name} = actor
        const actorList = document.createElement('li')
        actorList.insertAdjacentHTML('afterbegin',`
        <h3>${name}</h3>`)
        boxActors.append(actorList)
        })
    })

    filmsBox.append(filmList)  
    filmList.append(boxActors)
    
  })
})
  
// При использовании AJAX нет необходимости обновлять каждый раз всю страницу, так как обновляется только ее конкретная часть. Это намного удобнее, так как не приходится долго ждать

// Полезен потому что:
//  1 .Возможность создания удобного Web-интерфейса
//  2. Активное взаимодействие с пользователем
//  3.Частичная перезагрузка страницы, вместо полной
//  4.Удобство использования