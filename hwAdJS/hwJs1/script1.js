class Employee{
    constructor(name,age,salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    
    set name(value){
        this._name =value;
    }
    get name(){
        return this._name
    }
    set age(value){
        this._age =value;
    }
    get age(){
        return this._age
    }
    set salary(value){
        this._salary =value;
    }
    get salary(){
        return this._salary
    }
   
}
class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super (name,age,salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary * 3;
    }
}

const men2 = new Programmer('Alex', '30', 2000, 'en,ua');
const men3 = new Programmer('Pavel', '35', 4000, 'esp,ua');
const men4 = new Programmer('Andry', '32', 2500, 'pl');

console.log(men2,men3,men4, );
console.log(men2.salary)
console.log(men3.salary);
console.log(men4.salary);

