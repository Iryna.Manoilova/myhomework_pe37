// При открытии страницы, необходимо получить с сервера список всех пользователей и общий список публикаций. Для этого нужно отправить GET запрос на следующие два адреса:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// После загрузки всех пользователей и их публикаций, необоходимо отобразить все публикации на странице.
// Каждая публикация должна быть отображена в виде карточки (пример: https://prnt.sc/q2em0x), и включать заголовок, текст, а также имя, фамилию и имейл пользователя, который ее разместил.
// На каждой карточке должна присутствовать иконка или кнопка, которая позволит удалить данную карточку со страницы. При клике на нее необходимо отправить DELETE запрос по адресу https://ajax.test-danit.com/api/json/posts/${postId}. После получения подтверждения с сервера (запрос прошел успешно), карточку можно удалить со страницы, используя JavaScript.
// Более детальную информацию по использованию каждого из этих указанных выше API можно найти здесь.
// Данный сервер является тестовым. После перезагрузки страницы все изменения, которые отправлялись на сервер, не будут там сохранены. Это нормально, все так и должно работать.
// Карточки обязательно должны быть реализованы в виде ES6 классов. Для этого необходимо создать класс Card. При необходимости, вы можете добавлять также другие классы.

const userUrl = 'https://ajax.test-danit.com/api/json/users'
const postUrl = 'https://ajax.test-danit.com/api/json/posts'

class Card  {
   constructor(id, title, body, name, userName, email)  {
    this.id = id;
    this.name= name;
    this.userName = userName;
    this.title= title;
    this.body=body;
    this.email=email;
  }
}


Promise
    .all([fetch(userUrl),fetch(postUrl)])
    .then(([usersResp, postsResp]) => {
        return Promise
        .all([usersResp.json(), postsResp.json()])
    })
    .then(([usersList, postsList]) => {
        const arrCards = prepareCards(usersList, postsList)
        return arrCards;
    })
    .catch( err => console.log('trouble in request data: ', err) )
    .then(cards => {
        const cardsBox = buildHTMLCards(cards)
        document.body.insertAdjacentElement('afterbegin', cardsBox);
    })
    .catch( err => console.log('something went wrong: ', err) )

    function buildHTMLCards(arrCards){
        const cardsBox = document.createElement('ul')
    
        arrCards.forEach(card => {
            const {body, title,name,userName, email,id } = card
            cardsBox.insertAdjacentHTML('beforeEnd', `
            <div class ="card"  id="card-${id}">

            <span class="card_user">${name}   ${userName}</span>
            <span class="card_mail"><a href="mailto:${email}">${email}</a></span>
            <h1 class="card-title" > ${title}</h1>
            <div class="card_text">${body}</div>
            
            <div class="card_button">
                <button class="button_del" onclick="deletePost(${id})">delete</button>
            </div>
            </div>
            `)
         })
         return cardsBox;
    }

function deletePost(id){
     fetch (`https://ajax.test-danit.com/api/json/posts/${id}`,{
    method: 'DELETE'
    })
    .then(response => {
       if (response.status === 200){
            let el = document.getElementById(`card-${id}`);
            el.parentNode.removeChild(el)
        }
    })
    .catch( err => console.log('oppps: ', err) )
   
}

function prepareCards(users, posts){
    //prepare users object
    const indexedUsers= arrToIndexedObjById(users);

    return posts.map( post =>{
        const {id, body, title, userId} = post
        // get info about user from this post
        // const {name,username, email} = getUserByIdFromArr(userId, users)
        const {name,username, email} = indexedUsers[userId]
        const card = new Card (id, title, body,name,username, email)
        return card
    })
}
function arrToIndexedObjById (arr){
    const obj = {};
    arr.forEach(element => {
       obj[element.id] = element   
    });
    return obj
}



// function getUserByIdFromArr(id, users){
//   for (let i=0; i < users.length; i++ ){
//       if ( users[i].id === id){
//           return users[i]
//       }
//   }
// }




