let buttons = document.getElementsByClassName('btn');
let activeList = document.getElementsByClassName('active');

let btnsArr = Array.from(buttons).map((item)=> item);

window.addEventListener('keydown', function(event){
    let keyTouch = event.key.toLowerCase()
    let indexEl = btnsArr.findIndex((item)=>
         item.getAttribute('data-key') == keyTouch)
    
    if (indexEl !== -1) {
        Array.from(activeList).forEach((item)=> 
        item.classList.remove('active'))
        btnsArr[indexEl].classList.add('active')
    }
});
