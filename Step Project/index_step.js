/////////////////////////////////////////////////
/// tabs, section servises

let tabs = document.querySelectorAll('[data-tab]');
let active = document.getElementsByClassName('active');

const toggleContent = function() {
    if (!this.classList.contains("active")) {
        Array.from(active).forEach( item => {
            item.classList.remove('active');
        });
        
        let tabDataValue = this.getAttribute('data-tab');
        let tabContent = document.querySelector(`[data-content=${tabDataValue}]`);  

        this.classList.add('active');
        tabContent.classList.add('active');
    }
};

Array.from(tabs).forEach( item => {
  item.addEventListener('click', toggleContent);
});

/////////////////////////////////////////////////
let foto = document.querySelectorAll('[data-foto]');
let activeFoto = document.getElementsByClassName('active1');

const fotoContent = function() {
    if (!this.classList.contains("active1")) {
        Array.from(activeFoto).forEach( item => {
            item.classList.remove('active1');
        });
        
        let fotoDataValue = this.getAttribute('data-foto');
        let fotoContent = document.querySelector(`[data-box=${fotoDataValue}]`);  

        this.classList.add('active1');
        fotoContent.classList.add('active1');
    }
};

Array.from(foto).forEach( item => {
  item.addEventListener('click', fotoContent);
});



let buttonLeft = document.querySelector(".people_button.left")
let buttonRight = document.querySelector(".people_button.right")
let activeSlide = document.getElementsByClassName('slider_foto active1')
let slidesCount = foto.length


buttonLeft.addEventListener('click', function(){
    let numValue = activeSlide[0].getAttribute('data-num');
    let prevIndex = numValue -1

    if (prevIndex < 0){
        prevIndex = slidesCount -1
    }

    let prevSlide = document.querySelector(`[data-num='${prevIndex}']`)

    prevSlide.click()
}) ;

buttonRight.addEventListener('click', function(){
    let numValue = +activeSlide[0].getAttribute('data-num');
    let nextIndex = (numValue +1)%slidesCount
    let nextSlide= document.querySelector(`[data-num='${nextIndex}']`)
    
    nextSlide.click()
}) ;



let workBoxTabs = document.querySelectorAll('[data-workTabs]');
let activeWork = document.getElementsByClassName('active3');
let workBoxFoto = document.getElementsByClassName('work_foto_item');


const fillterContent=function(){
        
        if (!this.classList.contains("active3")) {
                Array.from(activeWork).forEach( item => {
                    item.classList.remove('active3');
                });
        this.classList.add('active3');

        Array.from(workBoxFoto).forEach(item => {
            item.classList.add('hide')
        });

        let tabsValue = this.getAttribute('data-workTabs');

        if (tabsValue == ('all')){
            Array.from(workBoxFoto).forEach(item => {
            item.classList.remove('hide')
            });
        }else{
            let boxFotoValue = document.querySelectorAll(`[data-workFoto=${tabsValue}]`);

            Array.from(boxFotoValue).forEach(item =>{
                item.classList.remove('hide')
            })
        }

    }



};
Array.from(workBoxTabs).forEach( item => {
    item.addEventListener('click', fillterContent);
  });


let buttonClick = document.querySelector('.workButton')
let countForOpen = 12

buttonClick.addEventListener('click', function(){
    let hiddenGallery =document.querySelectorAll('.work_foto .dnone')

    for( let i=0; i<countForOpen; i++){
        if(hiddenGallery[i] == undefined){
            buttonClick.classList.add('hide')
            break
        }else{
            hiddenGallery[i].classList.remove('dnone')
        }
    }

    if (hiddenGallery.length == countForOpen){
        buttonClick.classList.add('hide')
    }
})