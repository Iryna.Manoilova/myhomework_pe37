const price = document. createElement('input');
const conteiner = document. createElement('div');
const error = document.createElement('p');
const pricesConteiner = document.createElement('div');

conteiner.textContent= 'Price: ';
conteiner.prepend(pricesConteiner);
conteiner.append(price);
document.body.prepend(conteiner);


price.addEventListener('focus', function(event){
    event.target.style.cssText ='outline: 2px solid green';
    error.remove();
})

price.addEventListener('blur', function(event){
    const priseValue = +price.value;

    if(priseValue < 0 ){
        error.textContent = 'Please enter correct price';
        price.after(error);
        event.target.style.cssText = 'outline: 2px solid red';
       
    } else if (priseValue >= 0){
        error.remove();

        const p = document.createElement('p');
        const span = document.createElement('span');
        const button = document.createElement('button');

        p.style = ' display : inline-block; margin-right : 20px;border: 1px solid grey; padding:3px 5px; border-radius: 8px';
        price.style ='outline: 0px solid black; color:green';
        button.style = 'margin-left: 5px; border-radius:50%;'
        span.textContent = `Текущая цена:  ${priseValue}`;
        button.textContent = 'x';
                      
        p.append(span,button);
        pricesConteiner.prepend(p);
        
        button.addEventListener('click', function(event){
            p.remove();
            price.value = '';
            error.remove();
            price.style.cssText ='outline: 0px solid black';
        })
} else {
    // обработка всех остальных значениий
}
})
