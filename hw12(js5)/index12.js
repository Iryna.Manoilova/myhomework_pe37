function createNewUser() {
    let firstName = prompt ('Введите свое имя:')
    let lastName = prompt ('введите свою фамилию:')
    let birthday = prompt ('Введите дату рождения:','dd.mm.yyyy')

    const newUser = {
        _firstName:firstName, 
        _lastName:lastName,
        birthday:birthday,
        getLogin(){
            return (this._firstName[0] + this._lastName).toLowerCase();
         },
         getAge(){
            let trueFormatDate = birthday.split('.').reverse().join('.');
            return Math.floor((Date.now() - new Date(trueFormatDate))/(24*60*60*1000*365));

         },
         getPassword(){
            let passBirthday = new Date (birthday.split('.').reverse().join('.'));
            return (this._firstName[0]).toUpperCase() + (this._lastName).toLowerCase() + passBirthday.getFullYear();
         },
       
          get firstName(){
             return this._firstName;
          },
    
          get lastName(){
              return this._lastName;
          },
          
        setFirstName(valueFirstName){
            Object.defineProperty(newUser,"_firstName",{writable: true})
            this._firstName = valueFirstName;
            Object.defineProperty(newUser,"_firstName",{writable: false})
             },
        setLastName(valueLastName){
            Object.defineProperty(newUser,"_lastName",{writable: true})
            this._lastName = valueLastName;
            Object.defineProperty(newUser,"_lastName",{writable: false})
             }
    
        }
        Object.defineProperties(newUser,{
            "_firstName": {
            configurable: true,
            writable: false,
   
          },
            "_lastName":{
            configurable: true,
            writable: false,
         
          }
        });
        
    return newUser
    
    }
//   проверка того, что мы вводим через prompt
    const user = createNewUser();
    console.log (user.getLogin());
    console.log (user. getAge());
    console.log (user. getPassword());
    console.log (user.firstName);
    console.log (user.lastName);

    // изменение вводимых значений напрямую  
    user.lastName="Man";
    user.firstName="Alex";
    console.log (user.firstName);
    console.log (user.lastName);

// переназначение переменных с помощью сеттеров 

    user.setFirstName('Iryna');
    user.setLastName('Manoilova');
    console.log (user.firstName);
    console.log (user.lastName);





    
    