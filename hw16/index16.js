let tabs = document.querySelectorAll('[data-tab]');
let active = document.getElementsByClassName('active');


const toggleContent = function() {
    if (!this.classList.contains("active")) {
        Array.from(active).forEach( item => {
            item.classList.remove('active');
        });
        
        let tabDataValue = this.getAttribute('data-tab');
        let tabContent = document.querySelector(`[data-content=${tabDataValue}]`);  

        this.classList.add('active');
        tabContent.classList.add('active');
    }
};

Array.from(tabs).forEach( item => {
  item.addEventListener('click', toggleContent);
});